from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status, viewsets
from .serializers import FileSerializer
from .models import File

# Do I need this? or is this already handeled by Django?
#
# class FileViewPOST(APIView):
#    parser_classes = (MultiPartParser, FormParser)
#
#    def post(self, request, *args, **kwargs):
#        file_serializer = FileSerializer(data=request.data)
#        if file_serializer.is_valid():
#            file_serializer.save()
#            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
#        else:
#            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FileView(viewsets.ModelViewSet):
    queryset = File.objects.all()
    serializer_class = FileSerializer
