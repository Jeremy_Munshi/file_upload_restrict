from django.db import models
from private_storage.fields import PrivateFileField
from user.models import Profile


class File(models.Model):
    file = models.FileField(
        upload_to='%Y/%m/%d/uploads/', blank=False, null=False)
    # file = PrivateFileField("File", upload_to='%Y/%m/%d/uploads/',)
    info = models.CharField(max_length=300)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name="file")
    timestamp = models.DateTimeField(auto_now_add=True)
    share = models.ManyToManyField(Profile, related_name="shared", blank=True)

    def __str__(self):
        return '{0}'.format(self.info)
