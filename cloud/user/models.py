from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    about = models.TextField(verbose_name="About", max_length=500, blank=True)
    birth_date = models.DateField(
        verbose_name="Birth Date", null=True, blank=True)

    def __str__(self):
        return '{0}'.format(self.user.username)
